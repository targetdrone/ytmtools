from ytmusicapi import YTMusic
import time

yt = YTMusic('browser.json')
playlist = (yt.get_liked_songs(limit=800))

liked = playlist["tracks"]

songsinlist=[]
dupes=[]

for song in liked:
  for songinlistitem in songsinlist:
    if song["title"] == songinlistitem["title"] and song["artists"][0]["name"] == songinlistitem["artists"][0]["name"]:
      dupes.append(song)
  if song not in dupes:
    songsinlist.append(song)

for dupe in dupes:
  print(yt.rate_song(dupe['videoId'], 'INDIFFERENT'))
  print(dupe["title"])
  time.sleep(2)