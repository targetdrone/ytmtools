from ytmusicapi import YTMusic
import time

yt = YTMusic('browser.json')
playlist = (yt.get_liked_songs(limit=800))

liked = playlist["tracks"]

songsinlist=[]
dupes=[]

for song in liked:
  if song["title"] in songsinlist:
    dupes.append(song)
  else:
    songsinlist.append(song["title"])


for dupe in dupes:
  print(yt.rate_song(dupe['videoId'], 'INDIFFERENT'))
  time.sleep(2)